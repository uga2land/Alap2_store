<div class="panel panel-default sidebar-menu">
	<div class="panel-heading">
		<h3 class="panel-title">Kategori Produk</h3>
	</div>
	<!-- END PANEL HEADING -->
	<div class="panel-body">
		<ul class="nav nav-pills nav-stacked category_menu">
			<li><a href="shop.php">Jaket</a></li>
			<li><a href="shop.php">Aksesoris</a></li>
			<li><a href="shop.php">Sepatu</a></li>
			<li><a href="shop.php">Mantel</a></li>
			<li><a href="shop.php">Kaos</a></li>
		</ul>
		<!-- END NAV PILLS -->
	</div>
	<!-- END PANEL BODY -->
</div>
<!-- END PANEL --> 

<div class="panel panel-default sidebar-menu">
	<div class="panel-heading">
		<h3 class="panel-title">Kategori</h3>
	</div>
	<!-- END PANEL HEADING -->
	<div class="panel-body">
		<ul class="nav nav-pills nav-stacked category_menu">
			<li><a href="shop.php">Pria</a></li>
			<li><a href="shop.php">Wanita</a></li>
			<li><a href="shop.php">Anak-anak</a></li>
			<li><a href="shop.php">Lain-lain</a></li>
		</ul>
		<!-- END NAV PILLS -->
	</div>
	<!-- END PANEL BODY -->
</div>
<!-- END PANEL --> 