<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ALAPALAP</title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="style/bootstrap.min.css" rel="stylesheet">
	<link href="style/style.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>
	
	<?php

		include("includes/navbar.php");

	?>

	<div id="content">
		<div class="container">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Shop</li>
				</ul>
			</div>
			<!-- END COL-MD-12 -->
			<div class="col-md-3">
				<?php
					include("includes/sidebar.php");
				?>
			</div>
			<!-- END COL-MD-3 -->
			<div class="col-md-9">
				<div class="box">
					<h1>Shop</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ea voluptatum culpa impedit ipsum, ex, similique sed excepturi velit quidem, soluta itaque. Delectus nisi nihil culpa quam esse, corporis nesciunt in deserunt error quasi voluptatibus, eum non quae earum dolores.
					</p>
				</div>
				<!-- END BOX -->
				<div class="row">
					<div class="col-sm-4 col-sm-6 center-responsive">
						<div class="product">
							<a href="details.php">
								<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
								<p class="buttons">
									<a href="details.php" class="btn btn-default">View Details</a>
									<a href="details.php" class="btn btn-primary">
										<i class="fa fa-shopping-cart"></i> Add to Cart
									</a>
								</p>
							</div>
						</div>
					</div>
					<!-- END COL-SM-4 -->
					<div class="col-sm-4 col-sm-6 center-responsive">
						<div class="product">
							<a href="details.php">
								<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
								<p class="buttons">
									<a href="details.php" class="btn btn-default">View Details</a>
									<a href="details.php" class="btn btn-primary">
										<i class="fa fa-shopping-cart"></i> Add to Cart
									</a>
								</p>
							</div>
						</div>
					</div>
					<!-- END COL-SM-4 -->
					<div class="col-sm-4 col-sm-6 center-responsive">
						<div class="product">
							<a href="details.php">
								<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
								<p class="buttons">
									<a href="details.php" class="btn btn-default">View Details</a>
									<a href="details.php" class="btn btn-primary">
										<i class="fa fa-shopping-cart"></i> Add to Cart
									</a>
								</p>
							</div>
						</div>
					</div>
					<!-- END COL-SM-4 -->
					<div class="col-sm-4 col-sm-6 center-responsive">
						<div class="product">
							<a href="details.php">
								<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
								<p class="buttons">
									<a href="details.php" class="btn btn-default">View Details</a>
									<a href="details.php" class="btn btn-primary">
										<i class="fa fa-shopping-cart"></i> Add to Cart
									</a>
								</p>
							</div>
						</div>
					</div>
					<!-- END COL-SM-4 -->
					<div class="col-sm-4 col-sm-6 center-responsive">
						<div class="product">
							<a href="details.php">
								<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
								<p class="buttons">
									<a href="details.php" class="btn btn-default">View Details</a>
									<a href="details.php" class="btn btn-primary">
										<i class="fa fa-shopping-cart"></i> Add to Cart
									</a>
								</p>
							</div>
						</div>
					</div>
					<!-- END COL-SM-4 -->
					<div class="col-sm-4 col-sm-6 center-responsive">
						<div class="product">
							<a href="details.php">
								<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
								<p class="buttons">
									<a href="details.php" class="btn btn-default">View Details</a>
									<a href="details.php" class="btn btn-primary">
										<i class="fa fa-shopping-cart"></i> Add to Cart
									</a>
								</p>
							</div>
						</div>
					</div>
					<!-- END COL-SM-4 -->
				</div>
				<!-- END ROW -->
				<center>
					<ul class="pagination">
						<li><a href="shop.php">First Page</a></li>
						<li><a href="shop.php">2</a></li>
						<li><a href="shop.php">3</a></li>
						<li><a href="shop.php">4</a></li>
						<li><a href="shop.php">5</a></li>
						<li><a href="shop.php">6</a></li>
						<li><a href="shop.php">Last Page</a></li>
					</ul>
				</center>
			</div>
			<!-- END COL-MD-9 -->
		</div>
		<!-- END CONTAINER -->
	</div>
	<!-- END CONTENT -->

	<?php
	
		include("includes/footer.php");

	?>

   <script src="js/jquery-3.3.1.min.js"></script>
    
   <script src="js/bootstrap.min.js"></script>
</body>
</html>