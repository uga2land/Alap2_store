<center>
	<h1>Pesanan Saya</h1>
	<p class="lead">
		Semua pesanan Anda
	</p>
	<p class="text-muted">
		Jika ada pertanyaan silahkan <a href="../contact.php">hubungi</a> costumer service kami yang akan melayani anda 24/7 
	</p>
</center>

<hr>

<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>O N</th>			
				<th>Total Belanja</th>			
				<th>Invoice No</th>			
				<th>Qty</th>			
				<th>Ukuran</th>				
				<th>Tanggal Pemesanan</th>			
				<th>Pelunasan</th>			
				<th>Status</th>			
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>#1</th>
				<td>Rp 50.000.00</td>
				<td>6669635</td>
				<td>2</td>
				<td>Small</td>
				<td>2018-10-04</td>
				<td>Belum Dibayar</td>
				<td>
				<a href="confirm.php" target="blank" class="btn btn-primary btn-sm">Konfirmasi Pelunasan</a></td>
			</tr>
			<tr>
				<th>#1</th>
				<td>Rp 50.000.00</td>
				<td>6669635</td>
				<td>2</td>
				<td>Small</td>
				<td>2018-10-04</td>
				<td>Belum Dibayar</td>
				<td>
				<a href="confirm.php" target="blank" class="btn btn-primary btn-sm">Konfirmasi Pelunasan</a></td>
			</tr>
			<tr>
				<th>#1</th>
				<td>Rp 50.000.00</td>
				<td>6669635</td>
				<td>2</td>
				<td>Small</td>
				<td>2018-10-04</td>
				<td>Belum Dibayar</td>
				<td>
				<a href="confirm.php" target="blank" class="btn btn-primary btn-sm">Konfirmasi Pelunasan</a></td>
			</tr>
		</tbody>
	</table>
</div>