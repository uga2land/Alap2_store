<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ALAPALAP</title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="style/bootstrap.min.css" rel="stylesheet">
	<link href="style/style.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>
	
	<?php

		include("includes/navbar.php");

	?>

	<div id="content">
		<div class="container">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>My Account</li>
				</ul>
			</div>
			<!-- END COL-MD-12 -->
			<div class="col-md-3">
				<?php
					include("includes/sidebar.php");
				?>
			</div>
			<!-- END COL-MD-3 -->
            <div class="col-md-9">
                <div class="box">
                    <h1 align="center">Please Confirm Your Payment</h1>
                    <form action="confirm.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="">Invoice No:</label>
                            <input type="text" class="form-control" name="invoice_no" required>
                        </div>
                        <div class="form-group">
                            <label for="">Amount Sent:</label>
                            <input type="text" class="form-control" name="amount_sent" required>
                        </div>
                        <div class="form-group">
                            <label for="">Select Payment Mode:</label>
                            <select name="payment_mode" class="form-control">
                                <option>Select Payment Mode</option>
                                <option>Genius</option>
                                <option>BNI</option>
                                <option>VISA</option>
                                <option>Western Union</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Transaction/Reference Id:</label>
                            <input type="text" class="form-control" name="ref_no" required>
                        </div>
                        <div class="form-group">
                            <label for="">code:</label>
                            <input type="text" class="form-control" name="code" required>
                        </div>
                        <div class="form-group">
                            <label for="">Payment Date:</label>
                            <input type="text" class="form-control" name="date" required>
                        </div>
                        <div class="text-center">
                            <button type="submit" name="confirm_payment" class="btn btn-primary btn-lg">
                            <i class="fa fa-user"></i> Confirm Payment
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END COL-MD-9 -->
        </div>
		<!-- END CONTAINER -->
	</div>
	<!-- END CONTENT -->

	<?php
	
		include("includes/footer.php");

	?>

   <script src="js/jquery-3.3.1.min.js"></script>
    
   <script src="js/bootstrap.min.js"></script>
</body>
</html>