<h1 align="center">Edit Account</h1>

<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="">Nama</label>
        <input type="text" name="c_name" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="">Email</label>
        <input type="text" class="form-control" name="c_email" required>
    </div>
    <div class="form-group">
        <label for="">Alamat</label>
        <input type="text" class="form-control" name="c_address" required>
    </div>
    <div class="form-group">
        <label for="">Kota</label>
        <input type="text" class="form-control" name="c_city" required>
    </div>
    <div class="form-group">
        <label for="">Negara</label>
        <input type="password" class="form-control" name="c_country" required>
    </div>
    <div class="form-group">
        <label for="">Kontak</label>
        <input type="text" class="form-control" name="c_contact" required>
    </div>
    <div class="form-group">
        <label for="">gambar</label>
        <input type="file" class="form-control" name="c_image" required>
        <br>
        <img src="customer_images/user-1.jpeg" alt="" class="img-responsive img-thumbnail" width="100" height="100">
    </div>
    <div class="text-center">
        <button name="update" class="btn btn-primary">
            <i class="fa fa-user-md"></i> Update
        </button>
    </div>
</form>