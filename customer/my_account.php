<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ALAPALAP</title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="style/bootstrap.min.css" rel="stylesheet">
	<link href="style/style.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>
	
	<?php

		include("includes/navbar.php");

	?>

	<div id="content">
		<div class="container">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Account</li>
				</ul>
			</div>
			<!-- END COL-MD-12 -->
			<div class="col-md-3">
				<?php
					include("includes/sidebar.php");
				?>
			</div>
			<!-- END COL-MD-3 -->
			<div class="col-md-9">
				<div class="box">
					<?php
						if(isset($_GET['my_orders'])) {
							include("my_orders.php");
						}

						if(isset($_GET['pay_offline'])) {
							include("pay_offline.php");
						}

						if(isset($_GET['edit_account'])) {
							include("edit_account.php");
						}
						
						if(isset($_GET['change_password'])) {
							include("change_password.php");
						}

						if(isset($_GET['delete_account'])) {
							include("delete_account.php");
						}
					?>
				</div>
			</div>
		</div>
		<!-- END CONTAINER -->
	</div>
	<!-- END CONTENT -->

	<?php
	
		include("includes/footer.php");

	?>

   <script src="js/jquery-3.3.1.min.js"></script>
    
   <script src="js/bootstrap.min.js"></script>
</body>
</html>