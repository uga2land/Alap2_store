<h1 align="center">Edit Your Account</h1>

<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="">Enter Current Password</label>
        <input type="password" class="form-control" name="old_password" required>
    </div>
    <div class="form-group">
        <label for="">Enter New Password</label>
        <input type="password" class="form-control" name="new_password" required>
    </div>
    <div class="form-group">
        <label for="">Enter Your New Password Again</label>
        <input type="password" class="form-control" name="new_password_again" required>
    </div>
    <div class="text-center">
        <button type="submit" name="submit" class="btn btn-primary">
            <i class="fa fa-user-md"></i> Change Password
        </button>
    </div>

</form>