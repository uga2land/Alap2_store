<div class="panel panel-default sidebar-menu">
	<div class="panel-heading">
		<center>
			<img src="customer_images/user.jpg" alt="" class="img-responsive">
		</center>
		<br>
		<h3 class="panel-title" align="center">
			Name: Rdn - kw2
		</h3>
	</div>
	<!-- END PANEL HEADING -->
	<div class="panel-body">
		<ul class="nav nav-pills nav-stacked">
			<li class="<?php if(isset($_GET['my_orders'])){echo "active";} ?>">
				<a href="my_account.php?my_orders"><i class="fa fa-list"></i> Pesanan Saya</a>
			</li>
			<li class="<?php if(isset($_GET['pay_offline'])){echo "active";} ?>">
				<a href="my_account.php?pay_offline"><i class="fa fa-bolt"></i> Bayar Offline </a>
			</li>
			<li class="<?php if(isset($_GET['edit_account'])){echo "active";} ?>">
				<a href="my_account.php?edit_account"><i class="fa fa-pencil"></i> Edit Account </a>
			</li>
			<li class="<?php if(isset($_GET['change_password'])){echo "active";} ?>">
				<a href="my_account.php?change_password"><i class="fa fa-user"></i> Ganti Password </a>
			</li>
			<li class="<?php if(isset($_GET['delete_account'])){echo "active";} ?>">
				<a href="my_account.php?delete_account"><i class="fa fa-trash-o"></i> Hapus Account </a>
			</li>
			<li>
				<a href="logout.php"><i class="fa fa-sign-out"></i> Keluar </a>
			</li>
		</ul>
	</div>
	<!-- END PANEL BODY -->
</div>
<!-- END PANEL -->