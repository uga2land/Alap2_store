<div id="top">
		<div class="container">
			<div class="col-md-6 offer">
				<a href="my_account.php" class="btn btn-danger btn-sm">
					Selamat datang: Radit!
				</a>
				<a href="../cart.php">
					Total Belanja: Rp. 25.000.00 Total Barang: 2
				</a>
			</div> <!-- END COL-MD-6 OFFER -->
			<div class="col-md-6">
				<ul class="menu">
					<li><a href="customer_register.php">Register</a></li>
					<li><a href="customer/my_account.php">Account</a></li>
					<li><a href="cart.php">Keranjang</a></li>
					<li><a href="checkout.php">Login</a></li>
				</ul>
			</div> <!-- END COL-MD-6 -->
		</div> <!-- END CONTAINER -->
	</div> <!-- END TOP -->

	<div id="navbar" class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<a href="../index.php" class="navbar-brand home">
				<img src="../admin_area/product_images/alap2store.png" alt="" class="img-responsive" style="max-width: 60px;">
				</a>  
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navigation">
					<span class="sr-only">Toggle Navigation</span>
					<i class="fa fa-align-justify"></i>
				</button> <!-- END TOGGLE BUTTON -->
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#search">
					<span class="sr-only">Toggle Search</span>
					<i class="fa fa-search"></i>
				</button>
			</div> <!-- END NAVBAR HEADER -->

			<div class="navbar-collapse collapse" id="navigation">
				<div class="padding-nav">
					<ul class="nav navbar-nav navbar-left">
						<li>
							<a href="../index.php">Home</a>
						</li>
						<li>
							<a href="../shop.php">Shop</a>
						</li>
						<li>
							<a href="my_account.php">Account</a>
						</li>
						<li>
							<a href="../cart.php">Keranjang</a>
						</li>
						<li>
							<a href="../contact.php">Hubungi Kami</a>
						</li>
					</ul>
				</div> <!-- END PADDING NAV -->

				<a href="../cart.php" class="btn btn-primary navbar-btn right">
					<i class="fa fa-shopping-cart"></i>
					<span>2 barang dalam keranjang</span>
				</a>
				
				<div class="navbar-collapse collapse right">
					<button class="btn navbar-btn btn-primary" type="button" data-toggle="collapse" data-target="#search">
						<span class="sr-only">Toggle search</span>
						<i class="fa fa-search"></i>
					</button>
				</div> <!-- navbar-collapse collapse right -->

				<div class="collapse clearfix" id="search">
					<form class="navbar-form" method="get" action="result.php">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="search" name="user_query" required>
							<span class="input-group-btn">
								<button class="btn btn-primary" type="submit" name="search" value="search">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
						<!-- input group -->
					</form>
					<!-- navbar-form -->
				</div>
			</div>
			<!-- END NAVBAR COLLAPSE -->
		</div>
		<!-- END CONTAINER -->
	</div>
	<!-- END NAVBAR -->