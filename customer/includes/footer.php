<div id="footer">
   <div class="container">
      <!-- <div class="row"> -->
         <div class="col-md-3 col-sm-6">
            <h4>Halaman</h4>
            <ul>
               <li><a href="cart.php">Keranjang</a></li>
               <li><a href="contact.php">Hubungi Kami</a></li>
               <li><a href="shop.php">Shop</a></li>
               <li><a href="checkout.php">Account</a></li>
            </ul>
            <hr>
            <h4>Halaman Pengguna</h4>
            <ul>
               <li><a href="checkout.php">Login</a></li>
               <li><a href="customer_register.php">Register</a></li>
            </ul>
            <hr class="hidden-md hidden-lg hidden-sm">
         </div>
         <div class="col-md-3 col-sm-6">
            <h4>Produk Terpopuler</h4>
            <ul>
               <li><a href="#">Jaket</a></li>
               <li><a href="#">Aksesoris</a></li>
               <li><a href="#">Sepatu</a></li>
               <li><a href="#">Mantel</a></li>
               <li><a href="#">Kaos</a></li>
            </ul>
            <hr class="hidden-md hidden-lg hidden-sm">
         </div>
         <div class="col-md-3 col-sm-6">
            <h4>Alamat Kami</h4>
            <p>
               <strong>Alap Alap Store Ltd.</strong>
               <br>
               Jalan Maengket VII
               <br>
               Depok
               <br>
               087880323280
               <br>
               uga2land@gmail.com
               <br>
               <a href="https://gitlab.com/uga2land" target="blank">https://gitlab.com/uga2land</a> 
            </p>
            <a href="contact.php">Hubungi Kami</a>
            <hr class="hidden-md hidden-lg hidden-sm">
         </div>
         <div class="col-md-3 col-sm-6">
            <h4>Berlangganan</h4>
            <p class="text-muted">
               Lorem ipsum dolor sit amet consectetur adipisicing elit. In neque nostrum repellendus corrupti eveniet saepe soluta aliquam sint adipisci praesentium.
            </p>
            <form action="" method="post">
               <div class="input-group">
                  <input ty pe="text" class="form-control" name="email">
                  <span class="input-group-btn">
                     <input type="submit" value="subscribe" class="btn btn-default">
                  </span>
               </div>
            </form>
            <hr>
            <h4>Social Media Kami</h4>
            <p class="social">
               <a href="#"><i class="fa fa-facebook"></i></a>
               <a href="#"><i class="fa fa-twitter"></i></a>
               <a href="#"><i class="fa fa-instagram"></i></a>
               <a href="#"><i class="fa fa-google-plus"></i></a>
               <a href="#"><i class="fa fa-envelope"></i></a>
            </p>
         </div>
         <!-- END col-md-3 col-sm-6 -->
      <!-- </div> -->
      <!-- END ROW -->
   </div>
   <!-- END CONTAINER -->
</div>
<!-- END FOOTER -->

<div id="copyright">
   <div class="container">
      <div class="col-md-6">
         <p class="pull-left">
            &copy; 2018 &mdash; All Rights Reserved
         </p>
      </div>
      <div class="col-md-6">
         <p class="pull-right">
            made with <i class="fa fa-heart"></i> <a href="">alap2</a>
         </p>
      </div>
   </div>
</div>