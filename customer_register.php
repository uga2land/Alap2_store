<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ALAPALAP</title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="style/bootstrap.min.css" rel="stylesheet">
	<link href="style/style.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>
	
	<?php

		include("includes/navbar.php");

	?>

	<div id="content">
		<div class="container">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Register</li>
				</ul>
			</div>
			<!-- END COL-MD-12 -->
			<div class="col-md-3">
				<?php
					include("includes/sidebar.php");
				?>
			</div>
			<!-- END COL-MD-3 -->
			<div class="col-md-9">
				<div class="box">
					<div class="box-header">
						<center>
							<h2>Register a new account</h2>
						</center>
					</div>
					<form action="customer_register.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" class="form-control" name="c_name" required>
						</div>
						<div class="form-group">
							<label for="">Email</label>
							<input type="text" class="form-control" name="c_email" required>
						</div>
						<div class="form-group">
							<label for="">Address</label>
							<input type="text" class="form-control" name="c_address" required>
						</div>
						<div class="form-group">
							<label for="">City</label>
							<input type="text" class="form-control" name="c_city" required>
						</div>
						<div class="form-group">
							<label for="">Country</label>
							<input type="password" class="form-control" name="c_country" required>
						</div>
						<div class="form-group">
							<label for="">Contact</label>
							<input type="text" class="form-control" name="c_contact" required>
						</div>
						<div class="form-group">
							<label for="">Password</label>
							<input type="password" class="form-control" name="c_pass" required>
						</div>
						<div class="form-group">
							<label for="">Upload Photo</label>
							<input type="file" class="form-control" name="c_image" required>
						</div>
						<div class="text-center">
							<button type="submit" name="register"	class="btn btn-primary">
								<i class="fa fa-user-md"></i> Register
							</button>
						</div>
					</form>
				</div>
			</div>
			<!-- END COL-MD-9 -->
		</div>
		<!-- END CONTAINER -->
	</div>
	<!-- END CONTENT -->
			<?php
				include("includes/footer.php");
			?>

    <script src="js/jquery-3.3.1.min.js"></script>
    
   	<script src="js/bootstrap.min.js"></script>
</body>
</html>