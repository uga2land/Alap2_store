<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ALAPALAP</title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="style/bootstrap.min.css" rel="stylesheet">
	<link href="style/style.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>
	
	<?php
	
		include("includes/navbar.php");
	
	?>

	<div id="content">
		<div class="container">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li><a href="shop.php">Shop</a></li>
					<li>Detail</li>
				</ul>
			</div>
			<!-- END COL-MD-12 -->
			<div class="col-md-3">
				<?php
					include("includes/sidebar.php");
				?>
			</div>
			<!-- END COL-MD-3 -->

			<div class="col-md-9">
				<div class="row" id="productMain">
					<div class="col-sm-6">
						<div id="mainImage">
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
									<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
									<li data-target="#myCarousel" data-slide-to="1"></li>
									<li data-target="#myCarousel" data-slide-to="2"></li>
								</ol>
								<div class="carousel-inner" role="listbox">
									
									<div class="item active">
										<center>
											<img class="img-responsive" src="admin_area/product_images/500x700.png" alt="slider images-1">
										</center>
									</div>
									<div class="item">
										<center>
											<img class="img-responsive" src="admin_area/product_images/500x700.png" alt="slider images-1">
										</center>
									</div>
									<div class="item">
										<center>
											<img class="img-responsive" src="admin_area/product_images/500x700.png" alt="slider images-1">
										</center>
									</div>

								</div>
								<!-- end carousel inner -->
								<a href="#myCarousel" class="left carousel-control" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
								</a>
								<!-- left control -->
								<a href="#myCarousel" class="right carousel-control" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
								</a>
								<!-- right control -->
							</div>
							<!-- END CAROUSEL -->

							
						</div>
						<!-- END MAIN IMAGE -->
					</div>
					<!-- END COL-SM-6 -->

					<div class="col-sm-6">
						<div class="box">
							<h1 class="text-center">Dummy Images</h1>
							<form action="details.php" method="post" class="form-horizontal">
								<div class="form-group">
									<label for="" class="col-md-5 control-label">Product Quantity</label>
									<div class="col-md-7">
										<select name="product_qty" class="form-control" id="">
											<option value="">1</option>
											<option value="">2</option>
											<option value="">3</option>
											<option value="">4</option>
											<option value="">5</option>
										</select>
									</div>
									<!-- END COL-MD-7 -->
								</div>
								<div class="form-group">
									<label for="" class="col-md-5 control-label">Product Size</label>
									<div class="col-md-7">
										<select name="product_size" class="form-control" id="">
											<option value="">Select Size</option>
											<option value="">Small</option>
											<option value="">Medium</option>
											<option value="">Large</option>
										</select>
									</div>
								</div>
								<p class="price">Rp 50.000.00</p>
								<p class="text-center buttons">
									<button class="btn btn-primary" type="submit">
										<i class="fa fa-shopping-cart"></i> Add to Cart
									</button>
								</p>
							</form>
							<!-- END FORM -->
						</div>
						<!-- END BOX -->
						<div class="row" id="thumbs">
							<div class="col-xs-4">
								<a href="#" class="thumb">
									<img class="img-responsive" src="admin_area/product_images/500x700.png" alt="">
								</a>
							</div>
							<div class="col-xs-4">
								<a href="#" class="thumb">
									<img class="img-responsive" src="admin_area/product_images/500x700.png" alt="">
								</a>
							</div>
							<div class="col-xs-4">
								<a href="#" class="thumb">
									<img class="img-responsive" src="admin_area/product_images/500x700.png" alt="">
								</a>
							</div>
						</div>
						<!-- END ROW -->
						
					</div>
					<!-- END COL-SM-6 -->
				</div>
				<!-- END ROW -->
				<div class="box" id="details">
					<p>
						<h4>Product details</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque incidunt accusantium quasi debitis iure ex autem eum, nobis dolorem ratione dolorum libero aliquam repellat aperiam beatae iusto consequatur. Illum nostrum optio cumque rerum sit corrupti veniam assumenda fuga sapiente dolor odio doloribus cum rem iste, iure provident praesentium minus ipsam, perferendis ipsum recusandae saepe quis. Eligendi incidunt possimus necessitatibus dicta!</p>
						<h4>Size</h4>
						<ul>
							<li>small</li>
							<li>medium</li>
							<li>large</li>
						</ul>
					</p>
					<hr>
				</div>
				<!-- END BOX -->
				<div class="row same-height-row"> <!-- NEED ATTENTION ! -->
					<div class="col-md-3 col-sm-6">
						<div class="box same-height headline">
							<h3 class="text-center">You also like these products</h3>
						</div>
					</div>
					<!-- end col-md-3 col-sm-6 -->
					<div class="center-responsive col-md-3 col-sm-6">
						<div class="product same-height">
							<a href="details.php">
								<img src="admin_area/product_images/500x700.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
							</div>
						</div>
					</div>
					<div class="center-responsive col-md-3 col-sm-6">
						<div class="product same-height">
							<a href="details.php">
								<img src="admin_area/product_images/500x700.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
							</div>
						</div>
					</div>
					<div class="center-responsive col-md-3 col-sm-6">
						<div class="product same-height">
							<a href="details.php">
								<img src="admin_area/product_images/500x700.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
							</div>
						</div>
					</div>
				</div>
				<!-- END SAME-HEIGHT-ROW -->
			</div>
			<!-- END COL-MD-9 -->

		</div>
		<!-- END CONTAINER -->
	</div>
	<!-- END CONTENT -->

	<?php
	
		include("includes/footer.php");

	?>

   <script src="js/jquery-3.3.1.min.js"></script>
    
   <script src="js/bootstrap.min.js"></script>
</body>
</html>