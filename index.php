<?php
	include("includes/db.php");
	include("functions/functions.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Alap2store</title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="style/bootstrap.min.css" rel="stylesheet">
	<link href="style/style.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>
	
	<?php
		include("includes/navbar.php");
	?>
	
	<div class="container" id="slider">
		<div class="col-md-12">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner" role="listbox">
					<?php
						// Dynamic Slider
						$get_slides = "select * from slider LIMIT 0,1";
						$run_slides = mysqli_query($con,$get_slides);
						while($row_slides = mysqli_fetch_array($run_slides)) {
							$slide_name = $row_slides['slide_name'];
							$slide_image = $row_slides['slide_image'];

							echo "
								<div class='item active'>
									<img src='admin_area/slide_images/$slide_image'>
								</div>
							";
						}
					?>
					<?php
						// Dynamic Slider

						$get_slides = "select * from slider LIMIT 1,2";
						$run_slides = mysqli_query($con,$get_slides);
						while($row_slides=mysqli_fetch_array($run_slides)) {
							$slide_name = $row_slides['slide_name'];
							$slide_image = $row_slides['slide_image'];

							echo "
								<div class='item'>
									<img src='admin_area/slide_images/$slide_image'>
								</div>
							";
						}
					?>
					<!-- <div class="item active">
						<img src="admin_area/slide_images/1200x500.png" alt="slider images-1">
					</div>
					<div class="item">
						<img src="admin_area/slide_images/1200x500.png" alt="slider images-2">
					</div>
					<div class="item">
						<img src="admin_area/slide_images/1200x500.png" alt="slider images-4">
					</div> -->
				</div>
				<!-- end carousel inner -->
				<a href="#myCarousel" class="left carousel-control" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
					<span class="sr-only">Previous</span>
				</a>
				<!-- left control -->
				<a href="#myCarousel" class="right carousel-control" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
					<span class="sr-only">Next</span>
				</a>
				<!-- right control -->

			</div>
			<!-- end carousel -->
		</div>
		<!-- end col md-12 -->
	</div>
	<!-- END CONTAINER SLIDER -->
	
	<div id="advantages">
		<div class="container">
			<div class="same-height-row">
				<div class="col-sm-4">
					<div class="box same-height">
						<div class="icon">
							<i class="fa fa-heart"></i>
						</div>
						<h3>
							<a href="#">Lorem Ipsum!</a>
						</h3>
						<p>
							Saepe ipsa nesciunt libero soluta,alias aspernatur!
						</p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="box same-height">
						<div class="icon">
							<i class="fa fa-tags"></i>
						</div>
						<h3>
							<a href="#">Best Price!</a>
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde, fugiat?
						</p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="box same-height">
						<div class="icon">
							<i class="fa fa-thumbs-up"></i>
						</div>
						<h3>
							<a href="#">100% Puas!</a>
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit.ullam perspiciatis tenetur?
						</p>
					</div>
				</div>
			</div>
			<!-- end same-height-row -->
		</div>
		<!-- end container -->
	</div>
	<!-- END ADVANTAGES -->

	<div id="hot">
		<div class="box">
			<div class="container">
				<div class="col-md-12">
					<h2>Update terbaru</h2>
				</div>
			</div>
			<!-- END CONTAINER -->
		</div>
		<!-- END BOX -->
	</div>
	<!-- END HOT -->

	<div id="content" class="container">
		<div class="row">
			<div class="col-sm-3 col-sm-6 single">
				<div class="product">
					<a href="details.php">
						<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="details.php">Dummy Images</a></h3>
						<p class="price">Rp 50.000.00</p>
						<p class="buttons">
							<a href="details.php" class="btn btn-default">View Details</a>
							<a href="details.php" class="btn btn-primary">
								<i class="fa fa-shopping-cart"></i> Add to Cart
							</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-sm-6 single">
				<div class="product">
					<a href="details.php">
						<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="details.php">Dummy Images</a></h3>
						<p class="price">Rp 50.000.00</p>
						<p class="buttons">
							<a href="details.php" class="btn btn-default">View Details</a>
							<a href="details.php" class="btn btn-primary">
								<i class="fa fa-shopping-cart"></i> Add to Cart
							</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-sm-6 single">
				<div class="product">
					<a href="details.php">
						<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="details.php">Dummy Images</a></h3>
						<p class="price">Rp 50.000.00</p>
						<p class="buttons">
							<a href="details.php" class="btn btn-default">View Details</a>
							<a href="details.php" class="btn btn-primary">
								<i class="fa fa-shopping-cart"></i> Add to Cart
							</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-sm-6 single">
				<div class="product">
					<a href="details.php">
						<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="details.php">Dummy Images</a></h3>
						<p class="price">Rp 50.000.00</p>
						<p class="buttons">
							<a href="details.php" class="btn btn-default">View Details</a>
							<a href="details.php" class="btn btn-primary">
								<i class="fa fa-shopping-cart"></i> Add to Cart
							</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-sm-6 single">
				<div class="product">
					<a href="details.php">
						<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="details.php">Dummy Images</a></h3>
						<p class="price">Rp 50.000.00</p>
						<p class="buttons">
							<a href="details.php" class="btn btn-default">View Details</a>
							<a href="details.php" class="btn btn-primary">
								<i class="fa fa-shopping-cart"></i> Add to Cart
							</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-sm-6 single">
				<div class="product">
					<a href="details.php">
						<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="details.php">Dummy Images</a></h3>
						<p class="price">Rp 50.000.00</p>
						<p class="buttons">
							<a href="details.php" class="btn btn-default">View Details</a>
							<a href="details.php" class="btn btn-primary">
								<i class="fa fa-shopping-cart"></i> Add to Cart
							</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-sm-6 single">
				<div class="product">
					<a href="details.php">
						<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="details.php">Dummy Images</a></h3>
						<p class="price">Rp 50.000.00</p>
						<p class="buttons">
							<a href="details.php" class="btn btn-default">View Details</a>
							<a href="details.php" class="btn btn-primary">
								<i class="fa fa-shopping-cart"></i> Add to Cart
							</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-sm-6 single">
				<div class="product">
					<a href="details.php">
						<img src="admin_area/product_images/300x350.png" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="details.php">Dummy Images</a></h3>
						<p class="price">Rp 50.000.00</p>
						<p class="buttons">
							<a href="details.php" class="btn btn-default">View Details</a>
							<a href="details.php" class="btn btn-primary">
								<i class="fa fa-shopping-cart"></i> Add to Cart
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- END ROW -->
	</div>
	<!-- END CONTENT CONTAINER -->

	<?php
	
		include("includes/footer.php");

	?>

   <script src="js/jquery-3.3.1.min.js"></script>
   <script src="js/bootstrap.min.js"></script>
</body>
</html>