<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ALAPALAP</title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="style/bootstrap.min.css" rel="stylesheet">
	<link href="style/style.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>
	
	<?php

		include("includes/navbar.php");

	?>

	<div id="content">
		<div class="container">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Shopping Cart</li>
				</ul>
			</div>
			<!-- END COL-MD-12 -->
			<div class="col-md-9" id="cart">
				<div class="box">
					<form action="cart.php" method="post" enctype="multipart-form-data">
						<h1>Shopping Cart</h1>
						<p class="text-muted">You currently have 3 item(s) in your cart.</p>
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">Product</th>
										<th>Quantity</th>
										<th>Unit Price</th>
										<th>Size</th>
										<th colspan="1">Delete</th>
										<th colspan="2">Sub Total</th>
									</tr>
								</thead>
								<tbody class="table-striped">
									<tr>
										<td>
											<img src="admin_area/product_images/300x350.png" alt="">
										</td>
										<td><a href="#">Dummy Images</a></td>
										<td>2</td>
										<td>Rp 50.000.00</td>
										<td>Large</td>
										<td><input type="checkbox" name="remove[]"></td>
										<td>Rp 100.000.00</td>
									</tr>
									<tr>
										<td>
											<img src="admin_area/product_images/300x350.png" alt="">
										</td>
										<td><a href="#">Dummy Images</a></td>
										<td>2</td>
										<td>Rp 50.000.00</td>
										<td>Large</td>
										<td><input type="checkbox" name="remove[]"></td>
										<td>Rp 100.000.00</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="5">Total</th>
										<th colspan="2">Rp 200.000.00</th>
									</tr>
								</tfoot>
							</table>
							<!-- END TABLE -->
						</div>
						<!-- END TABLE RESPONSIVE -->
						<div class="box-footer">
							<div class="pull-left">
								<a href="index.php" class="btn btn-default">
									<i class="fa fa-chevron-left"></i> Continue Shopping
								</a>
							</div>
							<div class="pull-right">
								<button class="btn btn-default" type="submit" name="update" value="Update Cart">
									<i class="fa fa-refresh"></i> Update Cart
								</button>
								<a href="checkout.php" class="btn btn-primary">
									proceed to checkout <i class="fa fa-chevron-right"></i>
								</a>
							</div>
						</div>
						<!-- END BOX FOOTER -->
					</form>
					<!-- END FORM -->
				</div>
				<!-- END BOX -->
				<div class="row same-height-row"> <!-- NEED ATTENTION ! -->
					<div class="col-md-3 col-sm-6">
						<div class="box same-height headline">
							<h3 class="text-center">You also like these products</h3>
						</div>
					</div>
					<!-- end col-md-3 col-sm-6 -->
					<div class="center-responsive col-md-3 col-sm-6">
						<div class="product same-height">
							<a href="details.php">
								<img src="admin_area/product_images/500x700.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
							</div>
						</div>
					</div>
					<div class="center-responsive col-md-3 col-sm-6">
						<div class="product same-height">
							<a href="details.php">
								<img src="admin_area/product_images/500x700.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
							</div>
						</div>
					</div>
					<div class="center-responsive col-md-3 col-sm-6">
						<div class="product same-height">
							<a href="details.php">
								<img src="admin_area/product_images/500x700.png" alt="" class="img-responsive">
							</a>
							<div class="text">
								<h3><a href="details.php">Dummy Images</a></h3>
								<p class="price">Rp 50.000.00</p>
							</div>
						</div>
					</div>
				</div>
				<!-- END SAME-HEIGHT-ROW -->
			</div>
			<!-- END CART -->
			<div class="col-md-3">
				<div class="box" id="order-summary">
					<div class="box-header">
						<h3>Order Summary</h3>
					</div>
					<p class="text-muted">
						Shipping and Additional cost are calculated
					</p>
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td>Order Subtotal</td>
									<th>Rp 200.000.00</th>
								</tr>
								<tr>
									<td>Shipping and handling</td>
									<td>Rp 0.00</td>
								</tr>
								<tr>
									<td>Tax</td>
									<th>Rp 0.00</th>
								</tr>
								<tr class="total">
									<td>Total</td>
									<th>Rp 200.000.00</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END COL-MD-3 -->
		</div>
		<!-- END CONTAINER -->
	</div>
	<!-- END CONTENT -->
	
	<?php
	
		include("includes/footer.php");

	?>

   <script src="js/jquery-3.3.1.min.js"></script>
    
   <script src="js/bootstrap.min.js"></script>
</body>
</html>